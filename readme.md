[JourneyMap Tools](https://bitbucket.org/TeamJM/journeymap-tools)
============================================================

A collection of command-line tools for use with files produced by [JourneyMap for Minecraft](http://journeymap.info).

Official releases are here: http://minecraft.curseforge.com/projects/journeymap-tools

If you have improvements or new features to contribue, feel free to make Pull Requests. Chatting with the TeamJM
developers in Espernet IRC #journeymap is highly suggested.  Before you change anything or submit code, however, be sure
to read the [License Information](docs/license.md).

News
============================================================
* **14 March 2016**: Code is visual-sourced with limited rights

[Change Log](docs/changelog.md)
============================================================

*See what's changed across versions.*

[License Information](docs/license.md)
============================================================

*Who owns this code and what can you do with it.*


Help Wanted
============================================================

Implementation of **[MapMerger](src/main/java/info/journeymap/tools/command/MapMerger.java)**:

* Initial code has been checked in, but not tested.  Please test, fix, PR!