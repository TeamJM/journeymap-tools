[JourneyMap Tools](https://bitbucket.org/TeamJM/journeymap-tools) Changelog
======================================================

**v0.4**

* Visible-sourced
* Refactored project, added copyright headers, etc.
* Created MapMerger command class with roughed-in code, hasn't been tested yet.

**v0.3**

* MapSaver implementation
* Released: http://mods.curse.com/mc-mods/minecraft/226005-journeymap-tools

**0.2 and earlier**

* Not released