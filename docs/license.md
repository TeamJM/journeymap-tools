[JourneyMap Tools](https://bitbucket.org/TeamJM/journeymap-tools) License Information
==========================================================

**Unless otherwise noted, all code in this journeymap-tools repository is Copyright (C) Techbrew. All Rights Reserved.**

*However, the following limited rights are granted to you:*

You MAY:
 + Modify source code or artifacts for personal use
 + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-tools repository.
    Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.

You MAY NOT:
 - Distribute source code or artifacts (whether modified or not) from the TeamJM/journeymap-tools repository.
 - Submit any code to the TeamJM/journeymap-tools repository with a different license than this one.
 - Use source code or artifacts from the repository in any way not explicitly granted by this license.


[PNGJ : PNG image IO Java library](https://github.com/leonbloy/pngj) License Information
==========================================================
Copyright 2009-2012 Hernán J. González . Licensed under Apache 2.0

The TeamJM/journeymap-tools repository includes source code (in some cases modified) from PNGJ.
All such code is located in the `src/main/java/ar/**` directory.
