/*
 * JourneyMap Tools (http://journeymap.info/JourneyMapTools)
 * http://bitbucket.org/TeamJM/journeymap-tools
 *
 * Copyright (c) 2014-2016 Techbrew.  All Rights Reserved.
 * The following limited rights are granted to you:
 *
 * You MAY:
 *  + Modify source code or artifacts for personal use
 *  + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-tools repository.
 *     Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.
 *
 * You MAY NOT:
 *  - Distribute source code or artifacts (whether modified or not) from the TeamJM/journeymap-tools repository.
 *  - Submit any code to the TeamJM/journeymap-tools repository with a different license than this one.
 *  - Use source code or artifacts from the repository in any way not explicitly granted by this license.
 */

package info.journeymap.tools.command;

import info.journeymap.tools.Main;
import info.journeymap.tools.model.MapType;
import info.journeymap.tools.model.RegionCoord;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Merge images from two world directories (journeymap/data/mp/worldname), presumably from different players
 * or different Minecraft instances.
 * <p/>
 * TODO:  Completely untested.  This has just been roughed in as a starting point.
 *
 * @author Techbrew
 */
public class MapMerger
{
    static Logger logger = Logger.getLogger(MapMerger.class.getSimpleName());

    private final File worldDir1;
    private final File worldDir2;
    private final File resultWorldDir;
    private final boolean favorNewer;
    private final Integer vSlice;
    private final int dimension;
    private final MapType mapType;

    private final boolean debug;

    /**
     * Constructor.  See showArgs().
     */
    public MapMerger(File worldDir1, File worldDir2, File resultWorldDir, boolean favorNewer, Integer vSlice, int dimension, MapType mapType, boolean debug)
    {
        this.worldDir1 = worldDir1;
        this.worldDir2 = worldDir2;
        this.resultWorldDir = resultWorldDir;
        this.favorNewer = favorNewer;
        this.vSlice = vSlice;
        this.dimension = dimension;
        this.mapType = mapType;
        this.debug = debug;
    }

    /**
     * Commandline entry point.
     *
     * @param args see showArgs()
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        if (args.length == 1)
        {
            showArgs();
            return;
        }

        File worldDir1;
        File worldDir2;
        File resultWorldDir;
        boolean favorNewer;
        Integer vSlice;
        int dimension;
        MapType mapType;

        boolean debug = false;

        try
        {
            int i = 1; // skip command name
            worldDir1 = new File(args[i++]);
            worldDir2 = new File(args[i++]);
            resultWorldDir = new File(args[i++]);
            favorNewer = Boolean.valueOf(args[i++]);
            vSlice = Integer.parseInt(args[i++]);
            if (vSlice < 0)
            {
                vSlice = null;
            }
            dimension = Integer.parseInt(args[i++]);
            mapType = MapType.valueOf(args[i++]);

            if (args.length - 1 == i)
            {
                debug = "debug".equals(args[i]);
            }

            if (!worldDir1.canRead() || !worldDir2.canRead())
            {
                throw new IllegalArgumentException("worldDir1, worldDir2 must both exist and be readable by current user");
            }

            if (!resultWorldDir.exists() && !resultWorldDir.mkdirs())
            {
                throw new IllegalArgumentException("resultWorldDir must be writable by current user");
            }

            if (!resultWorldDir.canWrite())
            {
                throw new IllegalArgumentException("resultWorldDir must be writable by current user");
            }

            if (worldDir1.equals(worldDir2) || worldDir1.equals(resultWorldDir) || worldDir2.equals(resultWorldDir))
            {
                throw new IllegalArgumentException("worldDir1, worldDir2 and resultWorldDir must all be different directories");
            }

        }
        catch (Throwable t)
        {
            logger.severe(t.toString());
            showArgs();
            return;
        }

        MapMerger mapMerger = new MapMerger(worldDir1, worldDir2, resultWorldDir, favorNewer, vSlice, dimension, mapType, debug);
        mapMerger.mergeFolders();
    }

    /**
     * Show args on commandline.
     */
    public static void showArgs()
    {
        Main.out("MapMerger Arguments:  \"<worldDir1>\" \"<worldDir2>\" \"<resultWorldDir>\" <favorNewer>");
        Main.out("\t  \"<worldDir1>\"        Path to data/sp/worldname of the primary map files");
        Main.out("\t  \"<worldDir2>\"        Path to data/sp/worldname of the secondary map files");
        Main.out("\t  \"<resultWorldDir>\"   Path to folder of the resulting merged files");
        Main.out("\t  <favorNewer>           Whether to favor newer files instead worldDir1 files.  One of the following: true,false");
        Main.out("\t  <slice>                Vertical slice to map.  Use -1 for the surface in Overworld.");
        Main.out("\t  <dimension>            Dimension ID.  1 for Nether, 0 for Overworld, 1 for End");
        Main.out("\t  <mapType>              One of the following: day,night,underground. End/Nether require underground");
        Main.out("\nExample Usage: MapMerger" +
                " \"C:\\\\Game\\\\.minecraft\\\\journeymap\\\\data\\\\sp\\\\OurWorld\"" +
                " \"C:\\\\otherplayer\\\\OurWorld\"" +
                " \"C:\\\\tmp\\\\MyWorld.png\" true -1 0 day");
    }

    /**
     * Merge folders and images.
     *
     * @return true on success
     */
    private boolean mergeFolders()
    {
        try
        {
            // Step 1:  Locate the dimension+maptype image dir (srcDir) in both worldDir1 and worldDir2
            File srcDir1 = RegionCoord.getImageDir(worldDir1, vSlice, dimension, mapType);
            File srcDir2 = RegionCoord.getImageDir(worldDir2, vSlice, dimension, mapType);

            // Step 2:  Create an equivalent relative path within the resultWorldDir (targetDir)
            File targetDir = RegionCoord.getImageDir(resultWorldDir, vSlice, dimension, mapType);

            // Step 3:  Generate a set of relative File paths which are the union of all images found both in srcDir1 and srcDir2
            Set<String> paths = getRelativeImagePaths(worldDir1, srcDir1);
            paths.addAll(getRelativeImagePaths(worldDir2, srcDir2));

            // Step 4:  For each path: If only one file exists between srcDir1 and srcDir2, simply copy it to targetDir
            //          Otherwise:
            //          1. Read both files into BufferedImages
            //          2. Write the favored image atop the secondary image using Graphics.drawImage()
            //          3. Save the result to a new file in targetDir

            for (String path : paths)
            {
                File image1 = new File(worldDir1, path);
                File image2 = new File(worldDir2, path);
                File target = new File(targetDir, path);
                if (image1.exists() && image2.exists())
                {
                    if (!favorNewer || image1.lastModified() >= image2.lastModified())
                    {
                        mergeFiles(image1, image2, target);
                        logger.info("Merged image from worldDir1 over worldDir2 for: " + target.toString());
                    }
                    else
                    {
                        mergeFiles(image2, image1, target);
                        logger.info("Merged image from worldDir2 over worldDir1 for: " + target.toString());
                    }
                }
                else if (image1.exists())
                {
                    Files.copy(image1.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
                    logger.info("Copied from worldDir1: " + target.toString());
                }
                else if (image2.exists())
                {
                    Files.copy(image2.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
                    logger.info("Copied from worldDir2: " + target.toString());
                }
            }

            return true;
        }
        catch (Throwable t)
        {
            logger.severe("Error merging folders: " + t);
            t.printStackTrace(System.err);
            return false;
        }
    }

    /**
     * Write targetFile with an image of secondaryImage overlaid with favoredImage.
     */
    private void mergeFiles(File favoredImage, File secondaryImage, File targetFile) throws Exception
    {
        BufferedImage result = ImageIO.read(favoredImage);
        Graphics2D g = result.createGraphics();
        g.setComposite(AlphaComposite.DstOver);
        g.drawImage(ImageIO.read(secondaryImage), null, 0, 0);
        g.dispose();

        ImageIO.write(result, "PNG", targetFile);
    }

    /**
     * Generate a set of file paths of PNGs in imagesDir (relative to worldDir)
     */
    private Set<String> getRelativeImagePaths(File worldDir, File imagesDir) throws Exception
    {
        // Look for pngs
        Set<String> paths = new HashSet<String>();
        File[] pngFiles = imagesDir.listFiles(new FilenameFilter()
        {
            @Override
            public boolean accept(File dir, String name)
            {
                return name.endsWith(".png");
            }
        });

        if (pngFiles != null)
        {
            String parentPath = worldDir.getCanonicalPath();
            for (File png : pngFiles)
            {
                paths.add(png.getCanonicalPath().replaceFirst(parentPath, ""));
            }
        }
        return paths;
    }
}
