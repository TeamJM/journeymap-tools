/*
 * JourneyMap Tools (http://journeymap.info/JourneyMapTools)
 * http://bitbucket.org/TeamJM/journeymap-tools
 *
 * Copyright (c) 2014-2016 Techbrew.  All Rights Reserved.
 * The following limited rights are granted to you:
 *
 * You MAY:
 *  + Modify source code or artifacts for personal use
 *  + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-tools repository.
 *     Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.
 *
 * You MAY NOT:
 *  - Distribute source code or artifacts (whether modified or not) from the TeamJM/journeymap-tools repository.
 *  - Submit any code to the TeamJM/journeymap-tools repository with a different license than this one.
 *  - Use source code or artifacts from the repository in any way not explicitly granted by this license.
 */

package info.journeymap.tools.command;

import info.journeymap.tools.Main;
import info.journeymap.tools.model.MapType;
import info.journeymap.tools.model.RegionCoord;
import info.journeymap.tools.util.PngjHelper;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Stitches all region image tiles into a single image, optionally rescaling.
 * Uses PNGJ to avoid keeping the entire result in memory.
 *
 * @author Techbrew
 */
public class MapSaver
{
    static Logger logger = Logger.getLogger(MapSaver.class.getSimpleName());

    final File worldDir;
    final Integer vSlice;
    final int dimension;
    final MapType mapType;
    final boolean showGrid;
    final int sourceTileSize;
    final int resultTileSize;
    File saveFile;
    long outputColumns;
    long outputRows;

    ArrayList<File> files;
    boolean debug;
    Integer userMinX;
    Integer userMaxX;
    Integer userMinZ;
    Integer userMaxZ;

    public MapSaver(File worldDir, File saveFile, int sourceTileSize, int resultTileSize, Integer vSlice, int dimension, MapType mapType, boolean showGrid, boolean debug)
    {
        super();
        this.worldDir = worldDir;
        this.saveFile = saveFile;
        this.sourceTileSize = sourceTileSize;
        this.resultTileSize = resultTileSize;
        this.vSlice = vSlice;
        this.dimension = dimension;
        this.showGrid = showGrid;
        this.mapType = mapType;
        this.debug = debug;
    }

    public static void main(String args[]) throws Exception
    {
        if(args.length==1)
        {
            showArgs();
            return;
        }

        File worldDir;
        File saveFile;
        Integer sourceTileSize;
        Integer resultTileSize;
        Integer vSlice;
        Integer dimension;
        Boolean showGrid;
        MapType mapType;
        boolean debug = false;

        try
        {
            int i = 1; // skip command name
            worldDir = new File(args[i++]);
            saveFile = new File(args[i++]);
            sourceTileSize = Integer.parseInt(args[i++]);
            resultTileSize = Integer.parseInt(args[i++]);
            vSlice = Integer.parseInt(args[i++]);
            if (vSlice < 0)
            {
                vSlice = null;
            }
            dimension = Integer.parseInt(args[i++]);
            showGrid = Boolean.valueOf(args[i++]);
            mapType = MapType.valueOf(args[i++]);

            if (args.length - 1 == i)
            {
                debug = "debug".equals(args[i]);
            }

        }
        catch (Throwable t)
        {
            logger.severe(t.toString());
            MapSaver.showArgs();
            return;
        }

        MapSaver mapSaver = new MapSaver(worldDir, saveFile, sourceTileSize, resultTileSize, vSlice, dimension, mapType, showGrid, debug);
        mapSaver.saveMap();
    }

    public static void showArgs()
    {
        Main.out("MapSaver Arguments:  \"<worldDir>\" \"<saveFileName>\" <sourceTileSize> <resultTileSize> <slice> <dimension> <showGrid> <mapType>");
        Main.out("\t  \"<worldDir>\"        Path to data/sp/worldname");
        Main.out("\t  \"<saveFileName>\"    Path of PNG image file to save.");
        Main.out("\t  <sourceTileSize>    Current tile size. Unless you've resized them, use 512.");
        Main.out("\t  <resultTileSize>    New tile size for output.  Use sourceTileSize to avoid resizing.");
        Main.out("\t  <slice>             Vertical slice to map.  Use -1 for the surface in Overworld.");
        Main.out("\t  <dimension>         Dimension ID.  1 for Nether, 0 for Overworld, 1 for End");
        Main.out("\t  <showGrid>          Whether to add a grid overlay.  One of the following: true,false");
        Main.out("\t  <mapType>           One of the following: day,night,underground. End/Nether require underground");
        Main.out("\nExample Usage: MapSaver \"C:\\\\Game\\\\.minecraft\\\\journeymap\\\\data\\\\sp\\\\MyWorld\" \"C:\\\\tmp\\\\MyWorld.png\" 512 512 -1 0 false day");
    }

    public static BufferedImage createBlankImage(int width, int height)
    {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2D = img.createGraphics();
        return img;
    }

    public static File getBlankImageFile(int size)
    {
        final File tmpFile = new File(".", String.format("blank%spx.png", size));
        if (!tmpFile.canRead())
        {
            BufferedImage image;
            image = createBlankImage(size, size);
            try
            {
                tmpFile.getParentFile().mkdirs();
                ImageIO.write(image, "png", tmpFile);
                tmpFile.setReadOnly();
                tmpFile.deleteOnExit();
            }
            catch (IOException e)
            {
                e.printStackTrace(System.err);
            }
        }
        return tmpFile;
    }

    /**
     * Prepares files to be merged
     */
    private boolean prepareFiles()
    {

        try
        {
            logger.info("Scanning for region tiles...");

            // Look for pngs
            File imageDir = RegionCoord.getImageDir(worldDir, vSlice, dimension, mapType);
            File[] pngFiles = imageDir.listFiles();

            if (pngFiles == null || pngFiles.length == 0)
            {
                logger.severe("Didn't find any PNG files in directory.");
                return false;
            }

            files = new ArrayList<File>(pngFiles.length);

            final Pattern tilePattern = Pattern.compile("([^\\.]+)\\,([^\\.]+)\\.png");
            Integer minX = null, minZ = null, maxX = null, maxZ = null;

            for (File file : pngFiles)
            {
                Matcher matcher = tilePattern.matcher(file.getName());
                if (matcher.matches())
                {
                    files.add(file);
                    Integer x = Integer.parseInt(matcher.group(1));
                    Integer z = Integer.parseInt(matcher.group(2));
                    if (minX == null || x < minX)
                    {
                        minX = x;
                    }
                    if (minZ == null || z < minZ)
                    {
                        minZ = z;
                    }
                    if (maxX == null || x > maxX)
                    {
                        maxX = x;
                    }
                    if (maxZ == null || z > maxZ)
                    {
                        maxZ = z;
                    }
                }
            }

            if (files.size() == 0 || minX == null || maxX == null || minZ == null || maxZ == null)
            {
                logger.severe("No region files to save in " + imageDir);
                return false;
            }

            // Restrict to user params if provided
            if (userMinX != null)
            {
                minX = Math.max(minX, userMinX);
            }
            if (userMinZ != null)
            {
                minZ = Math.max(minZ, userMinZ);
            }
            if (userMaxX != null)
            {
                maxX = Math.min(maxX, userMaxX);
            }
            if (userMaxZ != null)
            {
                maxZ = Math.min(maxZ, userMaxZ);
            }

            final int count = files.size();

            outputColumns = (maxX - minX) + 1;
            outputRows = (maxZ - minZ) + 1;

            logger.info(String.format("Region bounds found: [%d,%s] to [%d,%s], resulting in %d columns by %d rows ", minX, minZ, maxX, maxZ, outputColumns, outputRows));

            if (outputColumns < 1 || outputRows < 1)
            {
                logger.severe("One or more region file names is incorrect. Aborting.");
                return false;
            }

            if (outputColumns > 62500 || outputRows > 62500)
            {
                logger.severe("Check for and remove region files with significantly high/low numbers.");
                logger.severe("The area to map exceeds 32,000,000 blocks. Aborting.");
                return false;
            }

            long blanks = (outputColumns * outputRows) - count;
            logger.info(String.format("%d Region tiles found. %d blank tiles will be added to fill gaps.", count, blanks));

            if (blanks > count)
            {
                logger.warning("WARNING! The blank areas exceed the mapped ones. Check for and remove region files with significantly high/low numbers.");
            }

            File rfile;
            RegionCoord rc;
            files.clear();

            // Collect the files, add blanks where necessary
            for (int rz = minZ; rz <= maxZ; rz++)
            {
                for (int rx = minX; rx <= maxX; rx++)
                {
                    rc = new RegionCoord(worldDir, rx, vSlice, rz, dimension);
                    rfile = RegionCoord.getImageFile(rc, mapType);
                    if (!rfile.canRead())
                    {
                        files.add(getBlankImageFile(sourceTileSize));
                    }
                    else
                    {
                        files.add(rfile);
                    }
                }
            }

            logger.info(String.format("\tTotal area is %d regions wide by %d regions high (%d chunks x %d chunks)",
                    outputColumns, outputRows, outputColumns * 512 / 16, outputRows * 512 / 16));

            logger.info(String.format("\tCombined tile images equal to: %d px x %d px",
                    outputColumns * sourceTileSize, outputRows * sourceTileSize));

            if (sourceTileSize != resultTileSize)
            {
                logger.info(String.format("\tResulting map image will be:   %d px x %d px",
                        outputColumns * resultTileSize, outputRows * resultTileSize));
            }

            return true;

        }
        catch (Throwable t)
        {
            logger.severe("Error preparing files: " + t);
            t.printStackTrace(System.err);
        }

        return false;
    }

    /**
     * Use pngj to assemble region files.
     */
    public File saveMap()
    {

        long start = System.currentTimeMillis();

        if (!prepareFiles())
        {
            return null;
        }

        try
        {
            // Merge image files
            File[] fileArray = files.toArray(new File[files.size()]);
            PngjHelper.stitchFiles(fileArray, saveFile, (int) outputColumns, sourceTileSize, resultTileSize, showGrid);
            logger.info("Map saved: " + saveFile.getCanonicalPath()); //$NON-NLS-1$ //$NON-NLS-2$

        }
        catch (java.lang.OutOfMemoryError e)
        {
            logger.severe("Out Of Memory");
        }
        catch (Throwable t)
        {
            logger.severe(t.toString());
            return null;
        }
        finally
        {
            long millis = System.currentTimeMillis() - start;
            long ms = millis % 1000;
            int secs = (int) ((millis / 1000) % 60);
            int mins = (int) ((millis / 1000) / 60);

            logger.info(String.format("Elapsed: %d min, %d sec, %dms", mins, secs, ms));
            //logger.info(new SimpleDateFormat("'Elapsed: 'mm' min, 'ss' sec, 'SSS' ms'").format(new Date(millis)));
        }

        return saveFile;
    }
}
