/*
 * JourneyMap Tools (http://journeymap.info/JourneyMapTools)
 * http://bitbucket.org/TeamJM/journeymap-tools
 *
 * Copyright (c) 2014-2016 Techbrew.  All Rights Reserved.
 * The following limited rights are granted to you:
 *
 * You MAY:
 *  + Modify source code or artifacts for personal use
 *  + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-tools repository.
 *     Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.
 *
 * You MAY NOT:
 *  - Distribute source code or artifacts (whether modified or not) from the TeamJM/journeymap-tools repository.
 *  - Submit any code to the TeamJM/journeymap-tools repository with a different license than this one.
 *  - Use source code or artifacts from the repository in any way not explicitly granted by this license.
 */

package info.journeymap.tools.model;

import java.io.File;

/**
 * Encapsulates data needed to associate a specific Minecraft Region (16x16 chunks) in a world+dimension
 * with an image tile.  This mimics a similiar object used by JourneyMap itself.
 */
public class RegionCoord implements Comparable<RegionCoord>
{
    public final File worldDir;
    public final int regionX;
    public final int regionZ;
    public final Integer vSlice;
    public final int dimension;

    /**
     * Constructor.
     *
     * @param worldDir  Data directory of JourneyMap image tiles.  (Usually journeymap/data/sp/worldname)
     * @param regionX   Minecraft Region x coord
     * @param vSlice    Vertical chunk (slice) coord.  0=y[0-15], 1=y[16-31], etc.
     * @param regionZ   Minecraft Region z coord
     * @param dimension dimension id. 0=Overworld, -1=Nether, 1=End, etc.
     */
    public RegionCoord(File worldDir, int regionX, Integer vSlice, int regionZ, int dimension)
    {
        this.worldDir = worldDir;
        this.regionX = regionX;
        this.regionZ = regionZ;
        this.vSlice = vSlice;
        this.dimension = dimension;
    }

    public static File getImageDir(File worldDir, Integer vSlice, int dimension, MapType mapType)
    {
        // Fake coord gets us to the image directory
        RegionCoord fakeRc = new RegionCoord(worldDir, 0, vSlice, 0, dimension);
        return getImageDir(fakeRc, mapType);
    }

    public static File getImageDir(RegionCoord rCoord, MapType mapType)
    {
        File dimDir = new File(rCoord.worldDir, "DIM" + rCoord.dimension); //$NON-NLS-1$
        File subDir = null;
        if (rCoord.isUnderground())
        {
            subDir = new File(dimDir, Integer.toString(rCoord.getVerticalSlice()));
        }
        else
        {
            subDir = new File(dimDir, mapType.name());
        }
        if (!subDir.exists())
        {
            subDir.mkdirs();
        }
        return subDir;
    }

    public static File getImageFile(RegionCoord rCoord, MapType mapType)
    {
        StringBuffer sb = new StringBuffer();
        sb.append(rCoord.regionX).append(",").append(rCoord.regionZ).append(".png"); //$NON-NLS-1$ //$NON-NLS-2$
        return new File(getImageDir(rCoord, mapType), sb.toString());
    }

    public Boolean isUnderground()
    {
        return vSlice != null && vSlice != -1;
    }

    public Integer getVerticalSlice()
    {
        if (vSlice == null)
        {
            return -1;
        }
        else
        {
            return vSlice;
        }
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + getVerticalSlice();
        result = prime * result + regionX;
        result = prime * result + regionZ;
        result = prime * result
                + ((worldDir == null) ? 0 : worldDir.hashCode());
        result = prime * result + dimension;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        RegionCoord other = (RegionCoord) obj;
        if (dimension != other.dimension)
        {
            return false;
        }
        if (regionX != other.regionX)
        {
            return false;
        }
        if (regionZ != other.regionZ)
        {
            return false;
        }
        if (other.getVerticalSlice() != getVerticalSlice())
        {
            return false;
        }
        if (worldDir == null)
        {
            if (other.worldDir != null)
            {
                return false;
            }
        }
        else if (!worldDir.equals(other.worldDir))
        {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(RegionCoord o)
    {
        int cx = Double.compare(this.regionX, o.regionX);
        return (cx == 0) ? Double.compare(this.regionZ, o.regionZ) : cx;
    }
}
