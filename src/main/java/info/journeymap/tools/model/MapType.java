/*
 * JourneyMap Tools (http://journeymap.info/JourneyMapTools)
 * http://bitbucket.org/TeamJM/journeymap-tools
 *
 * Copyright (c) 2014-2016 Techbrew.  All Rights Reserved.
 * The following limited rights are granted to you:
 *
 * You MAY:
 *  + Modify source code or artifacts for personal use
 *  + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-tools repository.
 *     Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.
 *
 * You MAY NOT:
 *  - Distribute source code or artifacts (whether modified or not) from the TeamJM/journeymap-tools repository.
 *  - Submit any code to the TeamJM/journeymap-tools repository with a different license than this one.
 *  - Use source code or artifacts from the repository in any way not explicitly granted by this license.
 */

package info.journeymap.tools.model;

/**
 * Enumeration of the map types produced by JourneyMap.
 */
public enum MapType
{
    day,
    night,
    underground;
}
