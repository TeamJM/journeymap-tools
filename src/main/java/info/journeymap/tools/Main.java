/*
 * JourneyMap Tools (http://journeymap.info/JourneyMapTools)
 * http://bitbucket.org/TeamJM/journeymap-tools
 *
 * Copyright (c) 2014-2016 Techbrew.  All Rights Reserved.
 * The following limited rights are granted to you:
 *
 * You MAY:
 *  + Modify source code or artifacts for personal use
 *  + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-tools repository.
 *     Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.
 *
 * You MAY NOT:
 *  - Distribute source code or artifacts (whether modified or not) from the TeamJM/journeymap-tools repository.
 *  - Submit any code to the TeamJM/journeymap-tools repository with a different license than this one.
 *  - Use source code or artifacts from the repository in any way not explicitly granted by this license.
 */

package info.journeymap.tools;

import info.journeymap.tools.command.MapSaver;

/**
 * Entry-point for JourneyMap Tools.  This is intentionally simplistic, and causally employs a Command Pattern
 * to delegate arguments to the target command.
 */
public class Main
{
    public static final String VERSION = "@VERSION@";
    public static final String LOGFORMAT_PROP = "java.util.logging.SimpleFormatter.format";
    public static final String LOGFORMAT = "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS [%4$s] %3$s:  %5$s%6$s%n";

    /**
     * Command-line entry point.  Delegates to the target command.
     *
     * @param args
     */
    public static void main(String[] args)
    {
        // Init logging format
        String testLogFormat = System.getProperty(LOGFORMAT_PROP);
        if (testLogFormat == null || testLogFormat.equals(""))
        {
            System.setProperty(LOGFORMAT_PROP, LOGFORMAT);
        }

        out(String.format("\n== JourneyMapTools %s ==", VERSION));
        out("Use at your own risk. Always back up your files before use.\n");

        if (args.length > 0)
        {
            try
            {
                args[0] = args[0].toLowerCase();

                if (args[0].equals("mapsaver"))
                {
                    MapSaver.main(args);
                    return;
                }

                if (args[0].equals("mapmerger"))
                {
                    // TODO:
                    // MapMerger.main(args);

                    out("MapMerger hasn't been implemented yet");
                    return;
                }

            }
            catch (Throwable t)
            {
                error("Fatal error: " + t, t);
            }
        }

        showArgs();
    }

    public static void showArgs()
    {
        out("\t  MapSaver     Save all region tile images to a single file, with optional resizing.");
        out("\t  MapMerger    Merge tiles from two world directories.");
        out("\nRun the jar again with one of the above tool names (as the first argument) to get example usage.");
    }

    /**
     * Shows commandline feedback.  For now, just uses System.out.
     *
     * @param message text to display
     */
    public static void out(String message)
    {
        System.out.println(message);
    }

    /**
     * Shows commandline feedback.  For now, just uses System.err.
     *
     * @param message text to display
     */
    public static void error(String message)
    {
        System.err.println(message);
    }

    /**
     * Shows commandline feedback.  For now, just uses System.err.
     *
     * @param message text to display
     */
    private static void error(String message, Throwable t)
    {
        System.err.println(message);
        t.printStackTrace();
    }


}
