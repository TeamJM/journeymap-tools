/*
 * JourneyMap Tools (http://journeymap.info/JourneyMapTools)
 * http://bitbucket.org/TeamJM/journeymap-tools
 *
 * Copyright (c) 2014-2016 Techbrew.  All Rights Reserved.
 * The following limited rights are granted to you:
 *
 * You MAY:
 *  + Modify source code or artifacts for personal use
 *  + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-tools repository.
 *     Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.
 *
 * You MAY NOT:
 *  - Distribute source code or artifacts (whether modified or not) from the TeamJM/journeymap-tools repository.
 *  - Submit any code to the TeamJM/journeymap-tools repository with a different license than this one.
 *  - Use source code or artifacts from the repository in any way not explicitly granted by this license.
 */

package info.journeymap.tools.util;

import ar.com.hjg.pngj.*;
import ar.com.hjg.pngj.chunks.ChunkLoadBehaviour;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Encapsulates knowledge of PngJ
 *
 * @author mwoodman
 */
public class PngjHelper
{
    static Logger logger = Logger.getLogger(PngjHelper.class.getSimpleName());

    /**
     * Stitch multiple PNG files into a single file.
     *
     * @param sourceFiles Filenames of PNG files to tile
     * @param destFile    Destination PNG filename
     * @param tileColumns How many tiles per row?
     *                    <p/>
     *                    Original: https://code.google.com/p/pngj/wiki/Snippets
     */
    public static void stitchFiles(final File sourceFiles[], final File destFile, final int tileColumns, final int sourceSize, final int tileSize, boolean showGrid)
    {
        final int ntiles = sourceFiles.length;
        final int tileRows = (ntiles + tileColumns - 1) / tileColumns; // integer ceil
        final PngReader[] readers = new PngReader[tileColumns];
        final ImageInfo destImgInfo = new ImageInfo(tileSize * tileColumns, tileSize * tileRows, 8, true); // bitdepth, alpha
        final PngWriter pngw = FileHelper.createPngWriter(destFile, destImgInfo, true);
        pngw.getMetadata().setText("Author", "JourneyMap");
        pngw.getMetadata().setText("Comment", "http://journeymaptools.techbrew.net");

        final ImageLine destLine = new ImageLine(destImgInfo, ImageLine.SampleType.INT, false);
        final int lineLen = tileSize * 4; // 4=bytesPixel
        final int gridColor = 135;

        int destRow = 0;

        for (int ty = 0; ty < tileRows; ty++)
        {
            logger.info(String.format("Processing row %d of %d...", (ty + 1), tileRows));

            int nTilesXcur = ty < tileRows - 1 ? tileColumns : ntiles - (tileRows - 1) * tileColumns;
            Arrays.fill(destLine.scanline, 0);

            for (int tx = 0; tx < nTilesXcur; tx++)
            { // open several readers
                File sourceFile = sourceFiles[tx + ty * tileColumns];
                readers[tx] = getReader(sourceFile, sourceSize, tileSize);
                readers[tx].setChunkLoadBehaviour(ChunkLoadBehaviour.LOAD_CHUNK_NEVER);
                readers[tx].setUnpackedMode(false);
            }

            rowcopy:
            for (int srcRow = 0; srcRow < tileSize; srcRow++, destRow++)
            {
                for (int tx = 0; tx < nTilesXcur; tx++)
                {
                    ImageLine srcLine = readers[tx].readRowInt(srcRow); // read line
                    int[] src = srcLine.scanline;

                    // Overlay chunk grid
                    if (showGrid)
                    {
                        int skip = (srcRow % 16 == 0) ? 4 : 64;
                        for (int i = 0; i <= src.length - skip; i += skip)
                        {
                            src[i] = (src[i] + src[i] + gridColor) / 3;
                            src[i + 1] = (src[i + 1] + src[i + 1] + gridColor) / 3;
                            src[i + 2] = (src[i + 2] + src[i + 2] + gridColor) / 3;
                            src[i + 3] = 255;
                        }
                    }

                    int[] dest = destLine.scanline;
                    int destPos = (lineLen * tx);
                    try
                    {
                        System.arraycopy(src, 0, dest, destPos, lineLen);
                    }
                    catch (ArrayIndexOutOfBoundsException e)
                    {
                        logger.severe("Bad image data. Src len=" + src.length + ", dest len=" + dest.length + ", destPos=" + destPos);
                        break rowcopy;
                    }
                }
                pngw.writeRow(destLine, destRow); // write to full image
            }

            for (int tx = 0; tx < nTilesXcur; tx++)
            {
                readers[tx].end(); // close readers
            }
        }

        pngw.end(); // close writer
    }

    /**
     * Gets a PngReader from the params provided.
     *
     * @param sourceFile PNG file
     * @param sourceSize PNG file size
     * @param tileSize   Image tile size
     * @return reader
     */
    private static PngReader getReader(File sourceFile, int sourceSize, int tileSize)
    {
        if (sourceSize == tileSize)
        {
            return FileHelper.createPngReader(sourceFile);
        }
        else
        {
            BufferedImage source = null;
            try
            {
                source = ImageIO.read(sourceFile);
            }
            catch (IOException e)
            {
                logger.severe("Corrupt or unreadable image file: " + sourceFile);
                throw new RuntimeException(e);
            }
            BufferedImage out = new BufferedImage(tileSize, tileSize, source.getType());
            Graphics2D g2D = out.createGraphics();

            if (tileSize < sourceSize)
            {
                g2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                g2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
            else
            {
                g2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                g2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
                g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            }
            g2D.drawImage(source, 0, 0, tileSize, tileSize, null);
            g2D.dispose();

            final ByteArrayOutputStream output = new ByteArrayOutputStream()
            {
                @Override
                public synchronized byte[] toByteArray()
                {
                    return this.buf;
                }
            };
            try
            {
                ImageIO.write(out, "png", output);
            }
            catch (IOException e)
            {
                logger.severe("Couldn't resize image: " + sourceFile);
                e.printStackTrace();
            }
            ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray(), 0, output.size());

            return new PngReader(inputStream, sourceFile.getName());
        }
    }
}
